package guru.springframework.services;

/**
 * Created by jt on 5/24/17.
 */
//These have been "moved" to the config file
//@Service
//@Primary
//@Profile({"en","default"})
public class PrimaryGreetingService implements GreetingService {

    private GreetingRepository greetingRepository;

    public PrimaryGreetingService(GreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    @Override
    public String sayGreeting() {
        return greetingRepository.getEnglishGreeting();
    }
}
